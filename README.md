# Distributed Systems Dependability (ITS21)

![python v3](https://img.shields.io/badge/python-v3-%230374b5) ![Linux](https://img.shields.io/badge/OS-Linux-%23e4b600) ![players](https://img.shields.io/badge/players-Len%2C%20Vladik%2C%20Markus%2C%20Andreas-%231dbbd5)

|                  Normal                  |               with inject                |
| :--------------------------------------: | :--------------------------------------: |
| ![DEMO](./__Docs/Demo-peer_test_01.webm) | ![DEMO](./__Docs/Demo-peer_test_02.webm) |

## Nice to know

> Runs best on **Linux**, for correct output on console.
>
> > On windows you will maybe not get readable output

## How to use

- **Short version:**
  > - Only needs:
  >   - **-n** for the current **peer_id** who start the ui,
  >   - **-l** where the **log** should be store and
  >   - **-g** where to find the **json peers list** with the peer's definition
  > - The **host** and **port** will then read by the **peer_id** from the **json peers list**
  - `python3 ui.py -n <peer_id> -l <path for log> -g <path to json peer's file>`
    - Example: `python3 ui.py -n peer_1001 -l . -g ./connection_config.json`
- **Long version:**
  > - Also define with:
  >   - **-i** for the current peer **host** address and
  >   - **-p** for the current peer **port**
  - `python3 ui.py -n <peer_id> -i <host> -p <port> -l <path for log> -g <path to json peer's file>`
    - Example: `python3 ui.py -n peer_1001 -i 127.0.0.1 -p 8181 -l . -g ./connection_config.json`
- **Injection version:** (works with short and long version)
  > - Need also the two options:
  >   - **-m** for **msg_id**, which is a combination of the **peer_id** and the **number** of the message as **msg_id**
  >     - **Example**, when you will inject the **4th** message from **peer 1002** -> you need to enter **"peer_10024"**, which is **peer_id + msg_id**
  >   - **-b** for **bit_id**, on which position you will add the injection, the number you enter must be between **1 and 16**
  - `python3 ui.py -n <peer_id> -l <path for log> -g <path to json peer's file> -m <msg_id (peer_id + msg_id))> -b <bit_id (1-16)>`
    - Example: `python3 ui.py -n peer_1001 -l . -g ./connection_config.json -m "peer_10014" -b 4`

### **Json peers list**

> - The peer list which will **load on startup** the client is defined in a **json format**.
> - It **contains** all **peer's** with there's informations to **build up a connection** from middleware to middleware
>
> > - It is defined as **json-object** with a json object named **peers**, which is defined as **json-array** from **json-object** with following content:
> >   - **peer_id**
> >   - **host**
> >   - **port**

#### Default structure

```json
{
  "peers": [
    {
      "peer_id": <string>,
      "host": <string>,
      "port": <number>
    }
  ]
}

```

## Dependencies

- `sudo apt-get install python3-pip`
  - `pip3 install pyfiglet`
    > For nice to have startup print out on console ;)

---

---

## Fernlehre 1

### Überblick

> Aufgabe ist die Implementierung eines Peers, der uber die Fähigkeit zur
> zuverlässigen Group Communication mit einer Gruppe gleichartiger Peers
> verfugt. Die folgenden Annahmen werden hierbei getroffen:

- Die Kommunikation der Peers erfolgt unter Annahme einer Closed
  Group, d. h. ein Peer muss auch in der Lage sein, seine eigenen
  Nachrichten zu empfangen.
- Jeder Peer verfügt uber eine einzigartige Peer-ID, die beim Starten des
  Peers via Option oder Argument konfiguriert wird. Es wird hierbei
  davon ausgegangen, dass die gleiche Peer-ID nicht mehr als einmal
  vergeben wird (d. h. eine automatische Erkennung von mehrfach
  vergebenen IDs ist nicht notwendig).
- Jeder Peer verfügt uber eine Liste aller Peer-IDs mit zugehöriger IP-Adresse
  und Portnummer des Peers, die als textuelle Konfigurationsdatei vorliegen
  und beim Starten des Peers via Option oder Argument angegeben wird.
  Es kann davon ausgegangen werden, dass sich diese Informationen
  während der Ausführung nicht ändert - d. h. es kann eine statische
  Gruppe ohne Notwendigkeit eines Membership Managements angenommen
  werden.
- Es wird angenommen, dass jeder Peer jeden anderen Peer direkt mittels
  UDP erreichen kann (kein Routing durch Peers erforderlich).
- Die Default-Gruppengröße (d. h. jene, mit der Tests erfolgreich ausfallen
  sollten) wird mit 5 Peers angenommen - prinzipiell sollte die Implementierung
  jedoch auch fur mehr oder weniger Peers funktionieren (ev. nach
  erneuter Kompilierung).

> Die nachfolgende Abbildung zeigt die grundlegende Architektur einer Peer-Implementierung,
> die aus folgenden Komponenten besteht:

- Ein User Interface (UI), das dem Benutzer ermöglichen soll, arbiträren
  Payload an die anderen Peers der Gruppe zu senden sowie empfangenen
  Payload in einer Datei abzuspeichern.
- Eine Middleware (MW), die Payload vom UI entgegennimmt und mittels
  zuverlässiger Group Communication an die anderen Peers als UDP-Datagramme
  versendet, sowie Nachrichten als UDP-Datagramme empfängt und den
  Payload dieser Nachrichten an das UI übergibt.
- Eine Error-Injection-Komponente in der Middleware, die zur Simulation
  von Errors in empfangenen Nachrichten dient (dies kann nicht auf der
  Ebene von UDP erfolgen, da Datagramme sonst bereits vom Netzwerkstack
  des OS verworfen werden würden).
- Das OS zum Senden und Empfangen von UDP-Datagrammen.

<img src="./__Docs/g68.png" height="400" width="auto">

> Die Anforderungen an die Implementierung und die einzelnen Komponenten
> werden im folgenden definiert.

### Anforderungen an die Implementierung - Allgemein

- Jede gesendete Nachricht soll mit einer einzigartigen Message-ID, bestehend
  aus der Peer-ID des Senders und einer fortlaufenden Sequenznummer
  versehen werden.
- Die Nachrichten sollen als Payload von UDP-Datagrammenübertragen
  werden, wobei eine geeignete Nachrichtenstruktur zu definieren ist. Für
  die Ansteuerung der UDP-Sockets sollen entsprechende Bibliotheken
  verwendet werden (z. B. POSIX Socket-API).
- Die Implementierung soll vollständig in einer der folgenden Programmiersprachen
  erfolgen:
  - C
  - C++
  - Python
  - Ada

### Anforderungen an die Komponente User Interface (UI)

- Das UI soll dem Benutzer eine interaktive, textbasierte Schnittstelle
  zur Verfügung stellen, die ausschließlich über das Terminal (d.h. die
  Shell) angesteuert wird.
- Sämtlicher angezeigter Text (d.h. Eingabeaufforderungen und Rückmeldungen)
  soll in Englisch verfasst sein.
- Das UI soll dem Benutzer jederzeit die Möglichkeit bieten, interaktiv
  einen arbiträren Payload mit einer frei definierbaren Maximallänge
  einzugeben, der mittels Group Communication an die anderen Peers
  gesendet werden soll.
- Das UI soll in der Lage sein, von der Middleware zugestellten Payload,
  der von einem anderen Peer empfangen wurde, inklusive der Message-ID
  in der Reihenfolge der Zustellung in einer Log-Datei abzuspeichern.
- Das UI soll in der Lage sein, die Konfiguration der Gruppe (Peer-ID
  und zugehörige IP-Adresse sowie Portnummer jedes Peers) über eine
  textuelle Konfigurationsdatei mit frei definierbarer Syntax einzulesen.
- Das UI soll die Möglichkeit bieten, folgende Einstellungen beim Starten
  des Peers mittels Optionen oder Argumenten zu konfigurieren:
  - die Peer-ID des Peers
  - die Portnummer des Peers
  - Der Pfad der Log-Datei
  - Der Pfad der Gruppen-Konfigurationsdatei
  - Angaben zur Error-Injection fur Nachrichten (siehe unten)

### Anforderungen an die Komponente Middleware (MW)

- Die Middleware soll eine zuverlässige Group Communication unter Erfüllung
  der im Skriptum angeführten Anforderungen (Integrity, Validity, Agreement)
  mittels iterativer Multicast Emulation (d. h. durch ein iteratives
  Senden an die Peers unter Verwendung von Unicast-Kommunikation)
  unterstützen. Die Datenübertragung zwischen jeweils zwei Peers soll
  dabei mittels Stop-and-Wait-ARQ realisiert werden (kein Sliding Window
  ARQ notwendig). Zwischen den Sendevorgängen an unterschiedliche
  Peers soll dabei eine konstante Wartezeit von 1 Sekunde abgewartet
  werden, um den Ausfall des Sende-Peers während der Übertragung
  testen zu können.
- Als Checksummen-Algorithmus für das Erkennen von Errors in den
  Nachrichten soll die Internet Checksum, definiert in RFC 1071, verwendet
  werden. Die Berechnung und Überprüfung der Checksumme soll dabei
  selbstständig implementiert werden (keine Verwendung von vorgefertigten
  Funktionen, Libraries, etc.).
- Die Anzahl maximaler Retransmissions während einer Stop-and-Wait-Übertragung
  soll auf einen Wert von 3 beschränkt werden.
- Die Middleware soll es dem Benutzer ermöglichen, zum Testen der
  Fehlertoleranzmechanismen, Errors in empfangene Nachrichten zu injecten.
  Dies soll durch Angabe der folgenden Informationen beim Starten des
  Peers erfolgen:

  - Angabe der Message-ID der zu modifizierenden Nachricht.
  - Angabe eines Bit-Index des zu toggelnden Bits der Nachricht zur
    Simulation eines Einzelbit-Errors.

  eine solch modifizierte Nachricht sollte dann beim Empfang durch die
  Checksummen-Überprüfung als fehlerhaft erkannt und vor der Verarbeitung
  durch die Middleware verworfen werden.

> Das gesamte Programm soll als Archiv mit allen zum Kompilieren /Ausführen
> notwendigen Quellcode/Script-Dateien bereitgestellt werden und ohne Fehler
> und Warnung durch aktuelle Versionen von gcc/gnatmake/dem Python-Interpreter
> kompiliert bzw. ausgeführt werden können.

### Die Beurteilung erfolgt nach folgenden Aspekten

- Korrektes Verhalten und Ausgabe der Implementierung unter Normalbedingungen
  und folgenden abnormalen Bedingungen:
  - Einzelbit-Errors in empfangenen Nachrichten.
  - Komplettausfall (silent failure) von ein oder mehreren Peers wahrend
    einer Datenübertragung.
- Korrektheit der Algorithmen (d.h. funktionieren diese prinzipiell für
  ALLE möglichen Situationen)
- Form, Struktur und Lesbarkeit des Quellcodes

> Alle zusätzlich getroffenen Annahmen, Einschränkungen (sofern mit dem
> Vortragenden vereinbart) und Erweiterungen sollen in einer geeigneten Dokumentation
> angeführt werden, die mit dem Quellcode-Archiv abzugeben ist.
