#!/usr/bin/python
# -*- coding: utf-8 -*-

# Made in Germany (Möge das neue Internet sein mit dir neu (Das Internet ist neuland))

import concurrent.futures
import marshal
import socket
import socketserver as SocketServer
import threading
import time
from concurrent.futures import Executor, ThreadPoolExecutor

from helper import Helper, Logger, Message_I, Peer_I, ThreadedUDPServer

####################################################################################################
####################################################################################################
####################################################################################################


# Variables
# ______________________________

# ______________________________
# ------------------------------
# CLASS:
# ______________________________
class MiddleWare:

    # Variables
    # ______________________________
    buffer_size = 1024
    executor = None
    threadSemaphore = threading.BoundedSemaphore(1)  # Semaphore for critical sections
    global_peer_queue = []

    given_bit_mask = None
    inject_msg_id = None

    # INIT
    # ______________________________
    def __init__(self, json_peer_array, host, port, peerId, callback_on_response, selected_msg_id, selected_bit_nr):
        try:
            self.executor = ThreadPoolExecutor(max_workers=1)
            self.peerId = peerId
            Logger.LOG_DEBUG("Init Middleware...")
            self.initInjection(selected_msg_id, selected_bit_nr)
            self.initPeerQueue(json_peer_array)
            self.initNewMessageHandler(host, port, callback_on_response)
            Logger.LOG_DEBUG("Middleware initialized!")
        except Exception as error:
            Logger.LOG_ERROR("[MiddleWare (__init__)]", error)

    # --------------------------------------------------------------------------------------------------
    ####################################################################################################
    ####################################################################################################
    ####################################################################################################
    # --------------------------------------------------------------------------------------------------

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    def initPeerQueue(self, json_peer_array):
        # global global_peer_queue
        try:
            if json_peer_array != None:
                # from json param choose the peers
                peer_list = json_peer_array["peers"]
                if peer_list != None:
                    # clear queue
                    self.global_peer_queue = []
                    # loop over all peers and append to queue
                    for peer in peer_list:
                        self.global_peer_queue.append(Peer_I(peer["peer_id"], peer["host"], peer["port"]))
        except Exception as error:
            self.global_peer_queue = None
            Logger.LOG_ERROR("[MiddleWare (initPeerQueue)]", error)

    ########################################################################
    #
    #
    #
    ########################################################################
    def initInjection(self, selected_msg_id, selected_bit_nr):
        try:
            if selected_msg_id != None and selected_bit_nr != None:
                self.inject_msg_id = selected_msg_id
                if selected_bit_nr != None:  # Set Bit Number from 1 to 16 for single bit manipulation on the 16bit IPChecksum
                    self.given_bit_mask = 0b1
                    self.given_bit_mask = self.given_bit_mask << int(selected_bit_nr) - 1 & 0xFFFF
                else:
                    self.given_bit_mask = 0
                Logger.LOG_INFO("Inject {} into MsgID {} Checksum".format(str(bin(self.given_bit_mask)), str(self.inject_msg_id)))
        except Exception as error:
            Logger.LOG_ERROR("[MiddleWare (initInjection)]", error)

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    def initNewMessageHandler(self, host, port, callback_on_response):
        # global global_peer_queue
        # global executorMain
        server = None
        try:
            if host != None and port != None and callback_on_response != None:
                # init UDP Server with callback function
                server = ThreadedUDPServer(
                    (host, port),
                    self.handlerFactory(self.global_peer_queue, callback_on_response, self.sendMessage, self.peerId, self.inject_msg_id, self.given_bit_mask),
                )
                # executorMain.submit(server.serve_forever)
                server_thread = threading.Thread(target=server.serve_forever)
                server_thread.daemon = True

                # start server listen
                server_thread.start()
                Logger.LOG_DEBUG("Server started at {} port {}".format(host, port))
                # while True: time.sleep(100)
        except (KeyboardInterrupt, SystemExit):
            Logger.LOG_ERROR("[MiddleWare (initNewMessageHandler)]", "Nice one 3!")
            if server != None:
                server.shutdown()
                server.server_close()
        except Exception as error:
            Logger.LOG_ERROR("[MiddleWare (initNewMessageHandler)]", error)

    def handlerFactory(self, peer_queue, callback_on_response, sendMessage, currentPeerId, inject_msg_id, given_bit_mask):
        try:

            def createHandler(*args, **keys):
                return ThreadedUDPRequestHandler(peer_queue, callback_on_response, sendMessage, currentPeerId, inject_msg_id, given_bit_mask, *args, **keys)

            return createHandler
        except (KeyboardInterrupt, SystemExit):
            pass
        except Exception as error:
            Logger.LOG_ERROR("[MiddleWare (handlerFactory)]", error)
        return None

    # --------------------------------------------------------------------------------------------------
    ####################################################################################################
    ####################################################################################################
    ####################################################################################################
    # --------------------------------------------------------------------------------------------------

    def sendMsg(self, sender_peer_id, msg):
        # global global_peer_queue
        try:
            # run over all peer's in the queue and send the msq
            if self.global_peer_queue != None:
                for peer in self.global_peer_queue:
                    # send msg only to current sender MW, who will start sending the message
                    if sender_peer_id == peer.peer_id:
                        # self.sendMessage(peer, peer, peer, msg)
                        self.executor.submit(self.sendMessage, peer, peer, peer, msg)
                        break
        except (KeyboardInterrupt, SystemExit):
            Logger.LOG_ERROR("[MiddleWare (sendMsg)]", "Nice one 4!")
        except Exception as error:
            Logger.LOG_ERROR("[MiddleWare (sendMsg)]", error)

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - by UI to send new Messages
    #   - by MW to send messages forward
    # params:
    # @peer_id_remote   : where to send the message
    # @peer_id_original : the original sender of this message
    # @peer_id_sender   : the current sender of this message
    # ______________________________________________________________
    def sendMessage(self, peer_remote, peer_original, peer_sender, msg):
        UDPClientSocket = None
        try:
            self.threadSemaphore.acquire()
            Logger.LOG_DEBUG("")
            Logger.LOG_DEBUG("########################################################################")
            Logger.LOG_DEBUG("    SEND PAYLOAD:")
            Logger.LOG_DEBUG("### - local original peer id: ....... %s" % peer_original.peer_id)
            Logger.LOG_DEBUG("### - local original arc_send: ...... %s" % peer_original.arq_send)
            Logger.LOG_DEBUG("### - local original arc_rec: ....... %s" % peer_original.arq_rec)
            Logger.LOG_DEBUG("### - local original msg id: ........ %s" % peer_original.msg_id)
            Logger.LOG_DEBUG("### - local original msg: ........... %s" % msg)
            Logger.LOG_DEBUG("------------------------------------------------------------------------")
            Logger.LOG_DEBUG("### - local sender peer id: ......... %s" % peer_sender.peer_id)
            Logger.LOG_DEBUG("### - local sender arc_send: ........ %s" % peer_sender.arq_send)
            Logger.LOG_DEBUG("### - local sender arc_rec: ......... %s" % peer_sender.arq_rec)
            Logger.LOG_DEBUG("### - local sender msg id: .......... %s" % peer_sender.msg_id)
            Logger.LOG_DEBUG("________________________________________________________________________")
            Logger.LOG_DEBUG("### - remote sender peer id: ........ %s" % peer_remote.peer_id)
            Logger.LOG_DEBUG("### - remote sender arc_send: ....... %s" % peer_remote.arq_send)
            Logger.LOG_DEBUG("### - remote sender arc_rec: ........ %s" % peer_remote.arq_rec)
            Logger.LOG_DEBUG("### - remote sender msg id: ......... %s" % peer_remote.msg_id)
            Logger.LOG_DEBUG("########################################################################")
            Logger.LOG_DEBUG("")
            self.threadSemaphore.release()

            # Create UPD client to send message
            UDPClientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            UDPClientSocket.settimeout(0.5)

            if UDPClientSocket != None:
                # create msg_id to send
                self.threadSemaphore.acquire()
                msg_id = None
                if peer_remote.peer_id == peer_sender.peer_id:
                    Logger.LOG_DEBUG("")
                    Logger.LOG_DEBUG("")
                    Logger.LOG_DEBUG("_______________________________________")
                    Logger.LOG_DEBUG(peer_sender.msg_id)
                    msg_id = peer_sender.msg_id + 1
                    Logger.LOG_DEBUG(peer_sender.msg_id)
                    Logger.LOG_DEBUG("_______________________________________")
                    Logger.LOG_DEBUG("")
                    Logger.LOG_DEBUG("")
                    UDPClientSocket.settimeout(5)
                else:
                    msg_id = peer_original.msg_id
                    UDPClientSocket.settimeout(1)
                self.threadSemaphore.release()

                # create payload for create checksum end send
                paylad = {
                    "OriginalPeerID": peer_original.peer_id,
                    "SenderPeerID": peer_sender.peer_id,
                    "ARQ": int(peer_remote.arq_send),
                    "MsgID": int(msg_id),
                    "Data": msg,
                }

                # create checksum over payload
                dataIPChecksum = Helper.inetChecksum(str(paylad))
                # add checksum to payload
                paylad["IPChecksum"] = dataIPChecksum
                # marshaling paylad to send over network
                paylad = marshal.dumps(paylad)

                # set retries state to 0
                retries = 0

                # and now we loop with 3 retries
                while retries <= 3:
                    try:
                        # send message to peer middleware
                        UDPClientSocket.sendto(paylad, (peer_remote.host, peer_remote.port))

                        received = UDPClientSocket.recv(self.buffer_size)
                        if received != None:
                            received_arq = None
                            try:
                                received_data = marshal.loads(received)
                                received_arq = int(received_data["ARQ"])
                            except Exception as error:
                                Logger.LOG_ERROR("[MiddleWare (sendMessage)] in while check ARQ received", error, 0)
                                break

                            if received_arq != None:
                                self.threadSemaphore.acquire()
                                if received_arq == peer_remote.arq_send:
                                    peer_remote.arq_send = (peer_remote.arq_send + 1) % 2
                                self.threadSemaphore.release()
                            break
                    except socket.timeout as error:
                        Logger.LOG_DEBUG("   - Timeout on try to send message to: {}".format(peer_remote.peer_id))
                    retries += 1
                if retries <= 3:
                    return True
        except (KeyboardInterrupt, SystemExit):
            Logger.LOG_ERROR("[MiddleWare (sendMessage)]", "Nice one 5!")
        except Exception as error:
            self.threadSemaphore.release()
            Logger.LOG_ERROR("[MiddleWare (sendMessage)]", error, 0)
        finally:
            # close socket after all
            if UDPClientSocket != None:
                UDPClientSocket.close()
        return False


####################################################################################################
####################################################################################################
####################################################################################################

# ______________________________
# ------------------------------
# CLASS:
# ______________________________
class ThreadedUDPRequestHandler(SocketServer.BaseRequestHandler):

    threadSemaphore = threading.BoundedSemaphore(1)  # Semaphore for critical sections

    # INIT
    # ______________________________
    def __init__(self, peer_queue, callback, sendMessage, currentPeerId, inject_msg_id, given_bit_mask, *args, **keys):
        try:
            # TODO: Current global peer is used,should get from MW
            self.peer_queue = peer_queue
            self.callback = callback
            self.sendMessage = sendMessage
            self.currentPeerId = currentPeerId

            self.inject_msg_id = inject_msg_id
            self.given_bit_mask = given_bit_mask
            self.executor = ThreadPoolExecutor(max_workers=3)
            SocketServer.BaseRequestHandler.__init__(self, *args, **keys)
        except Exception as error:
            Logger.LOG_ERROR("[ThreadedUDPRequestHandler (__init__)]", error, 0)

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    def handle(self):
        try:
            received = self.request[0]
            if received != None and self.request[1] != None:
                # un-marshal received data
                received_data = marshal.loads(received)
                if received_data != None:
                    # declare values which will response
                    received_original_peer_id = None
                    received_sender_peer_id = None

                    received_arq = None
                    received_msg_id = None

                    received_msg = None

                    received_ip_checksum = None

                    try:
                        if received_data["OriginalPeerID"] != None:
                            received_original_peer_id = received_data["OriginalPeerID"]
                        if received_data["SenderPeerID"] != None:
                            received_sender_peer_id = received_data["SenderPeerID"]

                        if received_data["ARQ"] != None:
                            received_arq = int(received_data["ARQ"])
                        if received_data["MsgID"] != None:
                            received_msg_id = int(received_data["MsgID"])
                        if received_data["Data"] != None:
                            received_msg = received_data["Data"]

                        if received_data["IPChecksum"] != None:
                            received_ip_checksum = received_data["IPChecksum"]

                        # check if all data are available in response
                        if (
                            received_original_peer_id != None
                            and received_sender_peer_id != None
                            and received_arq != None
                            and received_msg_id != None
                            and received_msg != None
                            and received_ip_checksum
                        ):
                            self.manageReceivedDataForAck(
                                received_original_peer_id, received_sender_peer_id, received_arq, received_msg_id, received_msg, received_ip_checksum
                            )
                    except Exception as error:
                        Logger.LOG_ERROR("[ThreadedUDPRequestHandler (handle)] Misconfigured packet received", error, 0)
        except Exception as error:
            Logger.LOG_ERROR("[ThreadedUDPRequestHandler (handle)]", error, 0)

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    def manageReceivedDataForAck(self, received_original_peer_id, received_sender_peer_id, received_arq, received_msg_id, received_msg, received_ip_checksum):
        # global global_peer_queue
        try:
            # Injection bit mask for IPChecksum manipulation
            if self.inject_msg_id != None and self.given_bit_mask != None:
                if self.inject_msg_id == (received_original_peer_id + str(received_msg_id)):
                    Logger.LOG_DEBUG("@@@ - Inject IPChecksum error")
                    Logger.LOG_DEBUG("@@@ - IPChecksum before: {}".format(str(bin(received_ip_checksum))))
                    received_ip_checksum = received_ip_checksum ^ self.given_bit_mask
                    Logger.LOG_DEBUG("@@@ - IPChecksum after: {}".format(str(bin(received_ip_checksum))))

            # create payload and verify IPChecksum
            paylad = {
                "OriginalPeerID": received_original_peer_id,
                "SenderPeerID": received_sender_peer_id,
                "ARQ": received_arq,
                "MsgID": received_msg_id,
                "Data": received_msg,
            }

            # verify the checksum
            if Helper.inetChecksum(str(paylad), int(received_ip_checksum)):
                Logger.LOG_ERROR("   Wrong checksum. NO ACK will be send!!!", "", 0)
            elif self.peer_queue != None:
                # Find Sender peer and original peer from queue by id's from response
                sender_original_peer = None  # the original sender of the message
                sender_sender_peer = None  # the current sender of the message (original or redirection)
                current_peer = None

                # get all needed peers from list by peer_id
                for peers in self.peer_queue:
                    if peers.peer_id == received_original_peer_id:
                        sender_original_peer = peers
                    if peers.peer_id == received_sender_peer_id:
                        sender_sender_peer = peers
                    if peers.peer_id == self.currentPeerId:
                        current_peer = peers

                    if sender_original_peer != None and sender_sender_peer != None and current_peer != None:
                        break

                if sender_original_peer != None and sender_sender_peer != None and current_peer != None:
                    self.threadSemaphore.acquire()
                    Logger.LOG_DEBUG("")
                    Logger.LOG_DEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                    Logger.LOG_DEBUG("    LOCAL PAYLOAD:")
                    Logger.LOG_DEBUG("||| - local original peer id: ....... %s" % sender_original_peer.peer_id)
                    Logger.LOG_DEBUG("||| - local original arc_send: ...... %s" % sender_original_peer.arq_send)
                    Logger.LOG_DEBUG("||| - local original arc_rec: ....... %s" % sender_original_peer.arq_rec)
                    Logger.LOG_DEBUG("||| - local original msg id: ........ %s" % sender_original_peer.msg_id)
                    Logger.LOG_DEBUG("-----------------------------------------------------------------------")
                    Logger.LOG_DEBUG("||| - local sender peer id: ......... %s" % sender_sender_peer.peer_id)
                    Logger.LOG_DEBUG("||| - local sender arc_send: ........ %s" % sender_sender_peer.arq_send)
                    Logger.LOG_DEBUG("||| - local sender arc_rec: ......... %s" % sender_sender_peer.arq_rec)
                    Logger.LOG_DEBUG("||| - local sender msg id: .......... %s" % sender_sender_peer.msg_id)
                    Logger.LOG_DEBUG("_______________________________________________________________________")
                    Logger.LOG_DEBUG("    RECEIVED PAYLOAD:")
                    Logger.LOG_DEBUG("+++ - received original peer id: .... %s" % received_original_peer_id)
                    Logger.LOG_DEBUG("+++ - received sender peer id: ...... %s" % received_sender_peer_id)
                    Logger.LOG_DEBUG("+++ - received arq: ................. %s" % received_arq)
                    Logger.LOG_DEBUG("+++ - received msg id: .............. %s" % received_msg_id)
                    Logger.LOG_DEBUG("+++ - received msg: ................. %s" % received_msg)
                    Logger.LOG_DEBUG("+++ - received ip checksum: ......... %s" % received_ip_checksum)
                    Logger.LOG_DEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                    Logger.LOG_DEBUG("")
                    self.threadSemaphore.release()

                    # create response with ARQ from sender to send back as ACK correct
                    response = {"ARQ": received_arq}
                    # marshaling response to send over network
                    response_data = marshal.dumps(response)
                    self.request[1].sendto(response_data, self.client_address)

                    self.manageReceivedDataToUpdatePeer(received_arq, received_msg_id, received_msg, sender_original_peer, sender_sender_peer, current_peer)
        except Exception as error:
            self.threadSemaphore.release()
            Logger.LOG_ERROR("[ThreadedUDPRequestHandler (manageReceivedData)]", error, 0)

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    def manageReceivedDataToUpdatePeer(self, received_arq, received_msg_id, received_msg, sender_original_peer, sender_sender_peer, current_peer):
        try:
            # verify ARQ from the current sender
            if received_arq == sender_sender_peer.arq_rec:
                # update ARQ from the current sender
                self.threadSemaphore.acquire()
                sender_sender_peer.arq_rec = (sender_sender_peer.arq_rec + 1) % 2
                self.threadSemaphore.release()

                # change the sender peer to the current one
                # original is the value which was response, not the current which is stay in the queue from current peer
                # that why we her create a new "origin"
                response_original_peer = Peer_I(sender_original_peer.peer_id, None, None)
                response_original_peer.setMore(None, received_arq, received_msg_id)

                # verify msg_id from original sender
                if received_msg_id > sender_original_peer.msg_id:
                    # update msg_id from original sender
                    self.threadSemaphore.acquire()
                    sender_original_peer.msg_id = received_msg_id
                    self.threadSemaphore.release()

                    # send now message to all other know peers
                    if self.sendRequestToAllPeers(response_original_peer, current_peer, received_msg):
                        # send message to UI over callback
                        self.callback(Message_I(sender_original_peer.peer_id, received_msg_id, received_msg))
                else:
                    self.threadSemaphore.acquire()
                    Logger.LOG_DEBUG("")
                    Logger.LOG_DEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                    Logger.LOG_DEBUG(
                        "??? - MSG already seen between {}({}) and {}({}) send from {} ({})!!!".format(
                            response_original_peer.peer_id,
                            response_original_peer.msg_id,
                            response_original_peer.peer_id,
                            response_original_peer.msg_id,
                            sender_sender_peer.peer_id,
                            sender_sender_peer.msg_id,
                        )
                    )
                    Logger.LOG_DEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                    Logger.LOG_DEBUG("")
                    self.threadSemaphore.release()
            else:
                self.threadSemaphore.acquire()
                Logger.LOG_DEBUG("")
                Logger.LOG_DEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                Logger.LOG_DEBUG(
                    "??? - ARQ not equals between {} ({}) and {} ({})!!!".format(
                        current_peer.peer_id, received_arq, sender_sender_peer.peer_id, sender_sender_peer.arq_rec
                    )
                )
                Logger.LOG_DEBUG("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                Logger.LOG_DEBUG("")
                self.threadSemaphore.release()
        except Exception as error:
            self.threadSemaphore.release()
            Logger.LOG_ERROR("[ThreadedUDPRequestHandler (manageReceivedDataToUpdatePeer)]", error, 0)

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    def sendRequestToAllPeers(self, peer_original, peer_sender, msg):
        # global global_peer_queue
        try:
            Logger.LOG_DEBUG("   - Sending to all other peers...")
            # countAllTrue = 0

            # add all peers with sendMessage class as thread to thread list
            for peer in self.peer_queue:
                if peer.peer_id != peer_sender.peer_id:
                    # countAllTrue+=1
                    # if self.sendMessage(peer, peer_original, peer_sender, msg):
                    #     countAllTrue-=1
                    # self.sendMessage(peer, peer_original, peer_sender, msg)
                    self.executor.submit(self.sendMessage, peer, peer_original, peer_sender, msg)
                    time.sleep(1)

            Executor().shutdown(wait=True)

            return True
        except (KeyboardInterrupt, SystemExit):
            Logger.LOG_ERROR("[ThreadedUDPRequestHandler (sendRequestToAllPeers)]", "Nice one 2!")
        except Exception as error:
            Logger.LOG_ERROR("[ThreadedUDPRequestHandler (sendRequestToAllPeers)]", error, 0)
        return False
