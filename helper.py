#!/usr/bin/python
# -*- coding: utf-8 -*-

# Made in Germany (Möge das neue Internet sein mit dir neu (Das Internet ist neuland))

"""
    ...
"""

import json
import os.path
import socketserver as SocketServer
import sys  # sys tools
import time
from json import JSONEncoder
from os import path

sys.path.insert(0, os.path.abspath(".."))

####################################################################################################
####################################################################################################
####################################################################################################

# ______________________________
# ------------------------------
# INTERFACE:
# ______________________________
class Peer_I:
    def __init__(self, peer_id, host, port):
        self.peer_id = peer_id
        self.host = host
        self.port = port
        self.arq_send = 0
        self.arq_rec = 0
        self.update = 0
        self.msg_id = 0

    def setMore(self, arq_send, arq_rec, msg_id):
        self.arq_send = arq_send
        self.arq_rec = arq_rec
        self.msg_id = msg_id


# ______________________________
# ------------------------------
# INTERFACE:
# ______________________________
class Message_I:
    def __init__(self, peer_id, msg_id, msg):
        self.peer_id = peer_id
        self.msg_id = msg_id
        self.msg = msg


class MessageEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, Message_I):
            return object.__dict__
        else:
            # call base class implementation which takes care of
            # raising exceptions for unsupported types
            return json.JSONEncoder.default(self, object)


# ______________________________
# ------------------------------
# CLASS:
# ______________________________
class ThreadedUDPServer(SocketServer.ThreadingMixIn, SocketServer.UDPServer):
    pass


# ______________________________
# ------------------------------
# CLASS:
# ______________________________
class Helper:
    logFileName = "testLog.json"  # filename where messages will be logged

    ################################################################
    ################################################################
    # read the log file with the always saved files and return them as a list of "Message" objects
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    @staticmethod
    def readLog(logPath, peer_id):
        try:
            Logger.LOG_DEBUG("Start read old logged messages...")

            p = "{}/{}-{}".format(logPath, peer_id, Helper.logFileName)
            if path.exists(p):
                f = None
                try:
                    f = open(p, "r")
                    if f.mode == "r":
                        # TODO: after load check if log json is correct/valid
                        jsonLog = json.loads(f.read())
                        resultLog = []
                        for log in jsonLog:
                            resultLog.append(Message_I(log["peer_id"], log["msg_id"], log["msg"]))
                        return resultLog
                    else:
                        Logger.LOG_DEBUG("Can not open log file to read!!!")
                except Exception as error:
                    Logger.LOG_ERROR("[Helper (readLog)] file has no content or not a correct json format", error, 0)
                finally:
                    if f != None:
                        f.close()
            else:
                Logger.LOG_DEBUG("Old log file not exist!")
        except Exception as error:
            Logger.LOG_ERROR("[Helper (readLog)]", error, 0)
        return None

    ################################################################
    ################################################################
    # write messages form logQueue as jsonstring into the log file
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    @staticmethod
    def writeLog(logPath, peer_id, logQueue):
        if path.exists(logPath):
            p = "{}/{}-{}".format(logPath, peer_id, Helper.logFileName)
            f = None
            try:
                f = open(p, "w+")
                if f.mode == "w+":
                    json_string = MessageEncoder().encode(logQueue)
                    # json_string = json.dumps(logQueue)
                    f.write(json_string)
                    return True
                else:
                    Logger.LOG_INFO("Can not open log file to read!!!", 1)
            except Exception as error:
                Logger.LOG_ERROR("[Helper (writeLog)]", error)
            finally:
                if f != None:
                    f.close()
        return False

    ################################################################
    ################################################################
    # DEFINE
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    @staticmethod
    def has_live_threads(self, threads):
        return True in [t.isAlive() for t in threads]

    ################################################################################################
    ################################################################################################
    ################################################################################################

    """
    Internet Checksum
    rfc1071.txt
    see lateblt.tribod.com/bit33.txt
    """

    ################################################################
    ################################################################
    # Take the lower 16 bit value from a 32 sum and add the higher value to it,
    # as long as the higher value is not null. Mask 16 bit with  & 0xFFFF
    # also on shifted 32 bit >> 16 bit because of unknowing behaver of shifting.
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    @staticmethod
    def sum32Reduce16(sum):
        while (sum >> 16) > 0:
            sum = (sum & 0xFFFF) + ((sum >> 16) & 0xFFFF)
        return sum

    ################################################################
    ################################################################
    # Calculate the rfc1071 checksum of the input data.
    # To verify checksum for data just run with expected checksum value, output 0 is a match!
    #
    # used:
    #   - ...
    # params:
    # ...
    # ______________________________________________________________
    @staticmethod
    def inetChecksum(data, checksum=0):
        # make 16 bit VALUE from two 8 bit word  and sum them
        # be sure not to loose values for overrun 16 bit
        # Mask for 16 bit low/high 8 bit because of unknowing behaver on shifting
        for i in range(0, len(data), 2):
            if i + 1 < len(data):
                checksum += ((ord(data[i]) << 8) & 0xFF00) + (ord(data[i + 1]) & 0xFF)
            else:
                checksum += ord(data[i]) & 0xFF

        # print hex(checksum) #debug output
        checksum = Helper.sum32Reduce16(checksum)

        # generate the one complement and return only a 16 bit value
        checksum = ~checksum & 0xFFFF
        # checksum = 0xFFFF - (checksum & 0xFFFF) # same like above

        return checksum


####################################################################################################
####################################################################################################
####################################################################################################

# ______________________________
# ------------------------------
# CLASS:
# ______________________________
class Logger:
    # Color Values
    # ______________________________
    ANSI_RESET = "\u001B[0m"
    ANSI_BLACK = "\u001B[30m"
    ANSI_RED = "\u001B[31m"
    ANSI_GREEN = "\u001B[32m"
    ANSI_YELLOW = "\u001B[33m"
    ANSI_BLUE = "\u001B[34m"
    ANSI_PURPLE = "\u001B[35m"
    ANSI_CYAN = "\u001B[36m"
    ANSI_WHITE = "\u001B[37m"

    # When to print
    # ______________________________
    printOnlyInfo = True
    printOnlyInfoButError = True

    @staticmethod
    def LOG_INFO(msg, isExit=0):
        print("{}[I] {}### {}{}".format(Logger.ANSI_YELLOW, Logger.ANSI_BLUE, Logger.ANSI_RESET, str(msg)), flush=True)
        if isExit == 1:
            sys.exit(2)

    @staticmethod
    def LOG_DEBUG(msg, isExit=0):
        if not Logger.printOnlyInfo:
            print("{}[D] {}### {}{}".format(Logger.ANSI_PURPLE, Logger.ANSI_BLUE, Logger.ANSI_RESET, str(msg)), flush=True)
            if isExit == 1:
                sys.exit(2)

    @staticmethod
    def LOG_ERROR(msg, error, isExit=1):
        if not Logger.printOnlyInfo or Logger.printOnlyInfoButError:
            print(
                "{}[E] {}{} {}--- {}{} {}### {}{} {}=== {}{}".format(
                    Logger.ANSI_RED,
                    Logger.ANSI_GREEN,
                    str(time.ctime()),
                    Logger.ANSI_BLUE,
                    Logger.ANSI_RESET,
                    str(msg),
                    Logger.ANSI_BLUE,
                    Logger.ANSI_RESET,
                    str(error),
                    Logger.ANSI_BLUE,
                    Logger.ANSI_RESET,
                    str(sys.exc_info()[0]),
                ),
                flush=True,
            )
            if isExit == 1:
                sys.exit(2)
