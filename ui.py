#!/usr/bin/python
# -*- coding: utf-8 -*-

# Made in Germany (Möge das neue Internet sein mit dir neu (Das Internet ist neuland))

"""Usage:
    my_program <peer-id> <host> <port> --log=<DIR> --peers=<DIR>

MSG-ID: peer-id-sender + seq-number

HINT:
* Response should save in a log file 
"""

import argparse  # arguments reader and parser
import json
import os.path
import re  # REGEX
import sys  # sys tools
import threading
import time
from os import path

from pyfiglet import figlet_format

from helper import Helper, Logger
from mw import MiddleWare

sys.path.insert(0, os.path.abspath(".."))


################################################################################################
################################################################################################
################################################################################################

########################################################################
#
# MAIN function
#
########################################################################


def main():
    try:
        print(figlet_format(" UI_v.1 ", font="slant"))
        print("#############################")
        print("---->      By:          <----")
        print("===>    Markus Witt      <===")
        print("===>   Lenhard Reuter    <===")
        print("===>   Andreas Schweiger <===")
        print("===> Vladislav Masepohl  <===")
        print("#############################")
        print()
        print()

        #############################
        # init UserInterface with defaults
        # ___________________________
        ui = UserInterface()

        Logger.LOG_DEBUG("----------------------------------------------------")
        Logger.LOG_DEBUG("")

        #############################
        # init Middleware with defaults
        #
        # start middleware listen on new messages
        # as params for the server listener are needed:
        # - the host for current UI starter (also listed in the json queue)
        # - the port for current UI starter (also listed in the json queue)
        # - the callback function from UI to print and save the incoming messages for the current UI/peer
        # ___________________________
        if ui.host != None and ui.port != None:
            mwInstance = MiddleWare(ui.peers, ui.host, ui.port, ui.peer_id, ui.printNewMessagesToConsole, ui.msg_id, ui.bit_id)
            # mwInstance.initInjection(ui.msg_id, ui.bit_id);

            #############################
            # start UI input handler
            # for sending messages, a send function from the MW will used
            # thats why we need here the MW instance function as callback
            # ___________________________
            ui.uiInputHandler(mwInstance.sendMsg)

            Logger.LOG_INFO("Everything is up, you can start now write messages :)")
        else:
            Logger.LOG_ERROR("Missing UI host and port arguments to init MW!!!", "Missing attributes")

        Logger.LOG_DEBUG("----------------------------------------------------")
        Logger.LOG_DEBUG("")
    except (KeyboardInterrupt, SystemExit):
        Logger.LOG_ERROR("[main (main)]", "Nice one!")
    except Exception as error:
        Logger.LOG_ERROR("[main (main)]", error)


################################################################################################
################################################################################################
################################################################################################

########################################################################
#
# UI Class
#
########################################################################
class UserInterface:

    ########################################################################
    # DEFAULT argument, definitions
    ########################################################################

    argumentList = [
        {"unix": "h", "gnu": "help", "required": True, "default": None, "help": "help"},
        {"unix": "n", "gnu": "peer_id", "required": True, "default": None, "help": "current peer-id"},
        {"unix": "i", "gnu": "host", "required": False, "default": None, "help": "host-address"},
        {"unix": "p", "gnu": "port", "required": False, "default": None, "help": "port"},
        {"unix": "l", "gnu": "log", "required": True, "default": None, "help": "log path"},
        {"unix": "g", "gnu": "peers", "required": True, "default": None, "help": "list of peers (json)"},
        {"unix": "m", "gnu": "msg_id", "required": False, "default": None, "help": "Unique message ID for injection "},
        {"unix": "b", "gnu": "bit_id", "required": False, "default": 0, "help": "Number of Bit to Manipulate (1-16^)"},
    ]

    peer_id = None
    logPath = None
    peers = None

    msg_id = 0
    bit_id = 0

    logQueue = None
    mwInstance = None

    ########################################################################
    #
    #
    #
    ########################################################################
    def __init__(self):
        try:
            Logger.LOG_DEBUG("Init UI...")

            #############################
            # try read arguments from console
            # and write them class global
            # ___________________________
            self.argumentParser()

            if self.peer_id != None and self.logPath != None and self.peers != None:
                # DEBUG
                Logger.LOG_DEBUG("")
                Logger.LOG_DEBUG("----------------------------------------------------")
                Logger.LOG_DEBUG("peer_id: {}".format(str(self.peer_id)))
                Logger.LOG_DEBUG("logPath: {}".format(str(self.logPath)))
                Logger.LOG_DEBUG("peers:   {}".format(str(self.peers)))
                Logger.LOG_DEBUG("----------------------------------------------------")
                Logger.LOG_DEBUG("")

                #############################
                # check if there are old log messages and print them
                # ___________________________
                self.checkOrCreateLogQueue()
                Logger.LOG_DEBUG("UI is init!")
            else:
                Logger.LOG_INFO("Failed init UI, missing attributes!!!")
        except Exception as error:
            Logger.LOG_ERROR("[UI (__init__)]", error)

    ########################################################################
    #
    # Calls msg log reader and fill the queue or create a new one
    # Also prints old messages directly to console
    #
    ########################################################################
    def checkOrCreateLogQueue(self):
        try:
            # check for old msg's
            # ___________________________
            if self.logPath != None:
                self.logQueue = Helper.readLog(self.logPath, self.peer_id)
            # when no found create new queue
            # ___________________________
            if self.logQueue == None:
                self.logQueue = []

            # print all now msg directly
            # ___________________________
            for msg in self.logQueue:
                self.printNewMessagesToConsole(msg, 1)
        except Exception as error:
            Logger.LOG_ERROR("[UI (checkOrCreateLogQueue)]", error)

    ########################################################################
    #
    # print new msg's to console
    # also add them to the global queue
    # and save the global queue directly to the msg log file
    #
    ########################################################################
    def printNewMessagesToConsole(self, msg, init=0):
        try:
            if self.logQueue != None and msg != None:
                # check by msg_id if the message is always known
                # only if not do something (or if the UI is start from new (init == 1) the print all saved messages)
                if not any(savedMsg.msg_id == msg.msg_id for savedMsg in self.logQueue) or init == 1:
                    if init == 0:
                        self.logQueue.append(msg)
                        if self.logPath != None:
                            Helper.writeLog(self.logPath, self.peer_id, self.logQueue)
                print("\n{}[M] {}{}: {}".format(Logger.ANSI_CYAN, Logger.ANSI_RESET, msg.peer_id, msg.msg))
        except Exception as error:
            Logger.LOG_ERROR("[UI (printNewMessagesToConsole)]", error)

    ########################################################################
    #
    # function to send message to all other peers
    #
    ########################################################################
    def uiInputHandler(self, mvCallbackSendMsg=None):
        readNewInput = True
        try:
            while readNewInput:
                userMsg = str(input(""))
                # userMsg = str(input("{}[M] {}YOU:".format(Logger.ANSI_CYAN, Logger.ANSI_RESET)))

                # On enter: exit
                # ___________________________
                if userMsg == "exit":
                    readNewInput = False
                # else input is message to send
                # ___________________________
                else:
                    if mvCallbackSendMsg != None and self.peer_id != None and userMsg != None:
                        # threading.Thread(target=mvCallbackSendMsg, args=(self.peer_id, userMsg)).start()
                        mvCallbackSendMsg(self.peer_id, userMsg)
                        # time.sleep(1)
        except Exception as error:
            readNewInput = False
            Logger.LOG_ERROR("[UI (uiInputHandler)]", error)

    ################################################################################################
    ################################################################################################
    ################################################################################################

    ########################################################################
    #
    # DEFAULT argument reader
    # peer_id => to identify the current UI starter
    # logPath => where to save the logged messages
    # peers   => the list of all known peers (also include starter peer (peer_id above))
    #
    ########################################################################
    def argumentReader(self, args):
        try:
            if args:
                peer_id = None
                host = None
                port = None

                logPath = None
                peers = None

                msg_id = None
                bit_id = None

                # peer_id
                # ___________________________
                if args.peer_id:
                    peer_id = args.peer_id
                else:
                    Logger.LOG_INFO("Missing arguments, type -h to see usage!", 1)
                # host
                # ___________________________
                if args.host:
                    if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", args.host) or args.host == "localhost":
                        host = args.host
                    else:
                        Logger.LOG_INFO("Not a valid host-address!!!", 1)
                # else:
                #     Logger.LOG_INFO("Missing arguments, type -h to see usage!", 1)
                # PORT
                # ___________________________
                if args.port:
                    if args.port.isdigit() and (int(args.port) > 1024 and int(args.port) < 10000):
                        port = int(args.port)
                    else:
                        Logger.LOG_INFO("Not a valid Port number!!!", 1)
                # else:
                #     Logger.LOG_INFO("Missing arguments, type -h to see usage!", 1)
                # LOG PATH
                # ___________________________
                if args.log:
                    if path.exists(str(args.log)):
                        logPath = args.log
                    else:
                        Logger.LOG_INFO("Path for log not exist!!!", 1)
                else:
                    Logger.LOG_INFO("Missing arguments, type -h to see usage!", 1)
                # PEER JSON LIST
                # ___________________________
                if args.peers:
                    if path.exists(str(args.peers)):
                        f = None
                        try:
                            f = open(args.peers, "r")
                            if f.mode == "r":
                                peers = json.loads(f.read())
                            else:
                                Logger.LOG_INFO("Can not open peers-config file to read!!!", 1)
                        except Exception as error:
                            Logger.LOG_ERROR("", error)
                        finally:
                            if f != None:
                                f.close()
                    else:
                        Logger.LOG_INFO("File of Peers-config not exist!!!", 1)
                else:
                    Logger.LOG_INFO("Missing arguments, type -h to see usage!", 1)
                # MSG_ID
                # ___________________________
                if args.msg_id:
                    msg_id = args.msg_id
                # BIT_ID
                # ___________________________
                if args.bit_id:
                    bit_id = args.bit_id

                if peer_id != None and host == None and port == None and peers != None:
                    for peer in peers["peers"]:
                        if peer["peer_id"] == peer_id:
                            host = peer["host"]
                            port = int(peer["port"])

                self.peer_id = peer_id
                self.host = host
                self.port = port

                self.logPath = logPath
                self.peers = peers

                self.msg_id = msg_id
                self.bit_id = bit_id
        except Exception as error:
            Logger.LOG_ERROR("[UI (argumentReader)]", error)

    ########################################################################
    #
    # DEFAULT argument parser
    #
    ########################################################################
    def argumentParser(self):
        try:
            # initiate the parser
            # ___________________________
            parser = argparse.ArgumentParser()

            # add long and short argument with help description
            # ___________________________
            for a in self.argumentList[1:]:
                parser.add_argument("-{}".format(a["unix"]), "--{}".format(a["gnu"]), help=a["help"], required=a["required"], default=a["default"])

            # read and return arguments from the command line
            # and start read arguments
            # ___________________________
            self.argumentReader(parser.parse_args())
        except Exception as error:
            Logger.LOG_ERROR("[UI (argumentParser)]", error)


########################################################################
#
# STARTUP/INIT function
#
########################################################################
if __name__ == "__main__":
    main()
